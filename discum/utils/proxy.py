from urllib.parse import urlparse, unquote


def parse_proxy_url(url):
    parsed = urlparse(url)

    scheme = parsed.scheme

    host = parsed.hostname
    if not host:
        raise ValueError('Empty host component')  # pragma: no cover

    try:
        port = parsed.port
    except (ValueError, TypeError):  # pragma: no cover
        raise ValueError('Invalid port component')

    try:
        username, password = (unquote(parsed.username),
                              unquote(parsed.password))
    except (AttributeError, TypeError):
        username, password = '', ''

    return scheme, host, port, username, password