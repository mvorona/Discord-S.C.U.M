import random, string
from ..RESTapiwrap import Wrapper


class Modal(object):
    __slots__ = ['discord', 's', 'log']

    def __init__(self, discord, s, log):
        self.discord = discord
        self.s = s
        self.log = log

    def submit(self, applicationID, channelID, guildID, nonce, data, sessionID):
        url = self.discord + "interactions"
        # nonce
        if nonce == "calculate":
            from ..utils.nonce import calculateNonce
            nonce = calculateNonce()
        else:
            nonce = str(nonce)
        # session id
        if sessionID == "random":
            sessionID = "".join(random.choices(string.ascii_letters + string.digits, k=32))
        # body
        body = {
            "type": 5,
            "application_id": applicationID,
            "channel_id": channelID,
            "guild_id": guildID,
            "data": data,
            "session_id": sessionID,
            "nonce": nonce
        }

        return Wrapper.sendRequest(self.s, 'post', url, body, log=self.log)
